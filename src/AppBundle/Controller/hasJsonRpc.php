<?php
/**
 * src/AppBundle/Controller/hasJsonRpc.php
 */

namespace AppBundle\Controller;

// Framework elements
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

// Business elements
use AppBundle\Entity\JrpcResponse;

/**
 * Has JSON-RPC
 * @see http://www.jsonrpc.org/specification
 */
trait hasJsonRpc
{
    /**
     * JSON RPC
     *
     * Checks the request for JSON-RPC parameters, and create a response object.
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param array $methods Allowed methods
     *
     * @return stdClass Parsed JSON-RPC request
     */
    protected function jRpc(Request $request)
    {
        // Raw POST data
        $jRpcRequest = json_decode($request->getContent());

        // Parse error
        if ($jRpcRequest === null) {
            $this->jRpcError(-32700);
        }

        if (is_array($jRpcRequest)) {
            // Request is an array of queries. Recurse over it.
            $response = array();
            foreach($jRpcRequest as $query) {
                $response[] = $this->jRpcHandleQuery($query);
            }
        } else {
            // Request is a single query.
            $response = $this->jRpcHandleQuery($jRpcRequest);
        }

        // Returns a new JSON-RPC response
        return new JsonResponse($response);
    }

    /**
     * The callbacks must be declared protected. Private ones wouldn't be
     * accessible.
     *
     * @param stdClass $request
     * @return JrpcResponse
     */
    protected function jRpcHandleQuery($request)
    {
        // JSON-RPC error -32600: Invalid request
        if ($request->jsonrpc != '2.0') {
            $this->jRpcError(-32600, null, $request->id);
        }

        if (method_exists($this, $request->method)) {
            // JSON-RPC method call
            $response = $this->{$request->method}(
                $request,
                new JrpcResponse($request->id)
            );
        } else {
            // JSON-RPC error -32601: Method not found
            $this->jRpcError(-32601, null, $request->id);
        }

        return $response;
    }

    /**
     * Creates a JSON-RPC error response, and sends it to the client.
     *
     * @param integer $code Error code
     * @param mixed $id JSON-RPC request id
     *
     * @return none
     */
    protected function jRpcError($code, $message = null, $id = null)
    {
        if ($message === null) {
            switch ($code) {
                case -32600: $message = 'Invalid request'; break;
                case -32601: $message = 'Method not found'; break;
                case -32700: $message = 'Parse error'; break;
                default: $message = 'Unknown error';
            }
        }

        $response = new JrpcResponse($id);

        $response->error = (object) array(
            'code'    => $code,
            'message' => $message
        );

        $response->send();

        return;
    }
}
