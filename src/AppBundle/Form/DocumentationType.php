<?php
/**
 * src/AppBundle/Form/DocumentationType.php
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Entity\Documentation;

/**
 * App bundle documentation type form
 *
 */
class DocumentationType extends AbstractType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return none
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('url')
        ;

        return;
    }

    /**
     * Configure options
     *
     * @param OptionsResolver $resolver
     *
     * @return none
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Documentation::class,
            'translation_domain' => false
        ));

        return;
    }
}
