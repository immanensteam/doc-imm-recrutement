<?php
/**
 * src/AppBundle/Form/TokenType.php
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Entity\Token;

/**
 * App bundle token type form
 *
 */
class TokenType extends AbstractType
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return none
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('token')
            ->add('ip')
            ->add('createdAt')
            ->add('availableUntil')
        ;

        return;
    }

    /**
     * Configure options
     *
     * @param OptionsResolver $resolver
     *
     * @return none
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Token::class,
            'translation_domain' => false
        ));

        return;
    }
}
