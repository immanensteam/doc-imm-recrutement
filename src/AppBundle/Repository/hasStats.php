<?php


namespace AppBundle\Repository;

/**
 * App bundle has stats trait
 */
trait hasStats
{
    /**
     * Count
     * 
     * @param string $alias
     * 
     * @return integer
     */
    public function count($alias)
    {
        $query = $this
            ->createQueryBuilder($alias)
            ->select('COUNT('.$alias.')')
        ;
        
        $result = $query
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $result;
    }
    
    /**
     * Weigh
     * 
     * @param string $schema
     * 
     * @return integer
     */
    public function weigh($schema)
    {
        $table = $this->_class->table['name'];
        
        $query = $this
            ->_em
            ->getConnection()
            ->query('SELECT data_length + index_length FROM information_schema.TABLES WHERE table_schema = "'.$schema.'" AND table_name = "'.$table.'"')
        ;
        
        $result = $query->fetch();
        
        return (int) current($result);
    }
}
