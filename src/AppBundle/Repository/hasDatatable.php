<?php
/**
 * src/AppBundle/Repository/hasDatatable.php
 */

namespace AppBundle\Repository;

use Doctrine\ORM\QueryBuilder;

/**
 * App bundle has datatable trait
 *
 */
trait hasDatatable
{
    /**
     * Fields select
     * 
     * Assembles a select SQL parameter from an associative array.
     * 
     * Example :
     * [
     *     'table1.field1' => 'fieldAlias1',
     *     'table1.field2' => null,
     * ]
     * 
     * Returns :
     * 
     * 'table.field1 AS fieldAlias1, table.field2'
     * 
     * @param stdClass $params
     * 
     * @return string
     */
    private function fieldsSelect($params)
    {
        $fields = $this->fields($params);
        
        $selection = '';
        
        foreach ($fields as $field => $alias) {
            $selection .= $field;
            
            if ($alias !== null) {
                $selection .= ' AS '.$alias;
            }
            
            $selection .= ', ';
        }
        
        return substr($selection, 0, -2);
    }
    
    /**
     * Search count
     * 
     * @param stdClass $params
     * @param string $alias
     * 
     * @return integer
     */
    public function searchCount($params, $alias)
    {
        $query = $this->searchQuery($params);
        
        $query->select('COUNT('.$alias.')');
        
        return (int) $query->getQuery()->getSingleScalarResult();
    }
    
    /**
     * DT search
     * 
     * @param QueryBuilder $query
     * @param stdClass $params
     * 
     * @return QueryBuilder
     */
    private function dtSearch(QueryBuilder &$query, &$params)
    {
        // Filters COUNTed fields from the list
        $fields = array_filter(array_keys($this->fields($params)), function($val) {
            return strpos(strtolower($val), 'count(') === false;
        });
        
        $searchString = join(' LIKE :search OR ', $fields).' LIKE :search';
        
        // Handles DataTable searching
        if (isset($params->search->value) && !empty($params->search->value)) {
            $query
                ->andWhere($searchString)
                ->setParameter('search', '%'.$params->search->value.'%')
            ;
        }
        
        return;
    }

    /**
     * DT sort
     * 
     * @param QueryBuilder $query
     * @param stdClass $params
     * 
     * @return QueryBuilder
     */
    private function dtSort(QueryBuilder &$query, &$params)
    {
        $i = 0;
        $fields = array();
        
        // Compatibility with aliased fields
        foreach($this->fields($params) as $field => $alias) {
            if ($alias === null) {
                $fields[$i++] = $field;
            } else {
                $fields[$i++] = $alias;
            }
        }
        
        // Handles DataTable sorting
        if (isset($params->order) && !empty($params->order)) {
            $query->orderBy(
                $fields[$params->order[0]->column],
                $params->order[0]->dir
            );
            if (count($params->order) > 1) {
                for ($i = 1, $c = count($params->order); $i < $c; $i++) {
                    $query->addOrderBy(
                        $fields[$params->order[$i]->column],
                        $params->order[$i]->dir
                    );
                }
            }
        }
        
        return $query;
    }

    /**
     * DT paginate
     * 
     * @param QueryBuilder $query
     * @param stdClass $params
     * 
     * @return QueryBuilder
     */
    private function dtPaginate(QueryBuilder &$query, &$params)
    {
        // Handles DataTable start record
        if (isset($params->start) && !empty($params->start)) {
            $query->setFirstResult($params->start);
        }
        
        // Handles DataTable records per page
        if (isset($params->length) && !empty($params->length)) {
            $query->setMaxResults($params->length);
        }
        
        return $query;
    }
}
