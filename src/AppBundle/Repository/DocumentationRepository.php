<?php
/*
 * src/AppBundle/Repository/User.php
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * App bundle documentation repository
 *
 */
class DocumentationRepository extends EntityRepository
{
    /**
     * hasTable defines :
     * - fieldsSelect()
     * - searchCount()
     */
    use hasDatatable;

    /**
     * hasStats defines :
     * - count()
     * - weigh()
     */
    use hasStats;

    /**
     * Fields
     *
     * @param stdClass $params
     *
     * @return array(field => alias|null)
     */
    private function fields($params)
    {
        switch ($params->request) {

            case 'admin':
                $fields = array(
                    'u.id'        => null,
                    'u.username'  => null,
                    'u.lastLogin' => null
                );
                break;
        }

        return $fields;
    }

    /**
     * Search query
     *
     * @param stdClass $params
     *
     * @return QueryBuilder
     */
    public function searchQuery($params)
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select($this->fieldsSelect($params))
            ->where('1 = 1')
        ;

        $this->dtSearch($query, $params);

        return $query;
    }

    /**
     * Search
     *
     * @param stdClass $params
     *
     * @return array
     */
    public function search($params)
    {
        $query = $this->searchQuery($params);

        $this->dtSort($query, $params);

        $this->dtPaginate($query, $params);

        return $query->getQuery()->getArrayResult();
    }
}
