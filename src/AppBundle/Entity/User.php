<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Token;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * doc-imm-login User entity
 *
 * @ORM\Entity(repositoryClass="\AppBundle\Repository\User")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToMany(targetEntity="Documentation")
     * @ORM\JoinColumn(name="documentation_id", referencedColumnName="id")
     */
    protected $documentations;


    public function __construct()
    {

        parent::__construct();
        $this->documentations = new ArrayCollection();

        return;
    }

    /**
     * To string
     *
     * @return string
     */
    public function __toString()
    {
        return 'User '.$this->getUsername();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getDocumentations()
    {
        return $this->documentations;
    }

    /**
     * @param mixed $documentations
     */
    public function setDocumentations($documentations)
    {
        $this->documentations = $documentations;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }



}