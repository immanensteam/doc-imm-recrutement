<?php
/**
 * src/DocIMM/DocumentationBundle/Form/DocumentationType.php
 */

namespace DocIMM\DocumentationBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;

use AppBundle\Form\DocumentationType as original;

// Foreign types
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * DocIMM documentation bundle Documentation type form
 */
class DocumentationType extends original
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return none
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        return;
    }
}
