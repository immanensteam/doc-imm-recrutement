<?php
/**
 * src/DocIMM/DocumentationBundle/Form/UserType.php
 */

namespace DocIMM\DocumentationBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;

use AppBundle\Form\UserType as original;

// Foreign types
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

/**
 * DocIMM  Documentation bundle user type form
 *
 */
class UserType extends original
{
    /**
     * Build form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return none
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('roles')
            ->add('roles', ChoiceType::class, array(
                'multiple' => true,
                'expanded' => true,
                'choices'  => array(
                    'Admin' => 'ROLE_ADMIN',
                    'User'    => 'ROLE_USER'

                )
            ))
            ->add('documentations', EntityType::class, [
                'class'     => 'AppBundle:Documentation',
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d');
                },
                'label'     => 'Access?',
                'expanded'  => true,
                'multiple'  => true,
            ])

        ;

        return;
    }
}
