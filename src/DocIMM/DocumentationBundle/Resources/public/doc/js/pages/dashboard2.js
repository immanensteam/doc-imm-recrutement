$(function () {

  'use strict';



var nbreq = $("#nb_requetes").text();

console.log(nbreq);
nbreq++;
console.log(nbreq);
//$("#nb_requetes").text() = nbreq;

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------

  //Faker Date
  var now = moment();

  var DateTab = [];
  var Week = 1;
  var Periode = 31;

  for(var i = 0 ; i < Periode; i++){

     // console.log('coucou'+i);
      if (moment().subtract(i, 'day').format("DD") == "01" || i == Periode - 1 ){

          DateTab[i] = moment().subtract(i, 'day').format("DD MMM");

      }else{
          DateTab[i] = moment().subtract(i, 'day').format("DD");
          Week++;
      }

  }


  DateTab.reverse();


    function randomize(lapse){

      var PeriodArray = [];  

      for(i=0;i<lapse;i++){

          PeriodArray[i] = Math.floor(Math.random() * 100000) + 1;

      }


      return  PeriodArray;

    }




  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $("#salesChart").get(0).getContext("2d");

  var salesChartData = {
    labels: DateTab,
    datasets: [
      {
        label: "Humains",
            fill: true,
            lineTension: 0.3,
            borderWidth: 0.5,
            backgroundColor: "rgba(0,166,90,0.8)",
            borderColor: "rgba(0,166,90,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.5,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 0.5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(0,166,90,1)",
            pointHoverBorderColor: "rgba(255,255,255,0.5)",
            pointHoverBorderWidth: 0.5,
            pointRadius: 0,
            pointHitRadius: 10,
            data: randomize(Periode),
            spanGaps: false,
      } ,
      {
        label: "Monétisés",
            fill: true,
            lineTension: 0.3,
            borderWidth: 0.5,
            backgroundColor: "rgba(0,249,135,0.8)",
            borderColor: "rgba(0,249,135,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.5,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 0.5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(255,255,255,0.5)",
            pointHoverBorderWidth: 0.5,
            pointRadius: 0,
            pointHitRadius: 10,
            data: randomize(Periode),
            spanGaps: false,
      }   ,
      {
        label: "Référencement",
            fill: true,
            lineTension: 0.3,
            borderWidth: 0.5,
            backgroundColor: "rgba(0,192,239,0.8)",
            borderColor: "rgba(0,192,239,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.5,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 0.5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(0,192,239,1)",
            pointHoverBorderColor: "rgba(255,255,255,0.5)",
            pointHoverBorderWidth: 0.5,
            pointRadius: 0,
            pointHitRadius: 10,
            data: randomize(Periode),
            spanGaps: false,
      },
      {
        label: "Suspects",
            fill: true,
            lineTension: 0.3,
            borderWidth: 0.5,
            backgroundColor: "rgba(255,133,27,0.8)",
            borderColor: "rgba(255,133,27,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.5,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 0.5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(255,133,27,1)",
            pointHoverBorderColor: "rgba(255,255,255,0.5)",
            pointHoverBorderWidth: 0.5,
            pointRadius: 0,
            pointHitRadius: 10,
            data: randomize(Periode),
            spanGaps: false,
      },
      {
        label: "Bad Bot",
            fill: true,
            lineTension: 0.3,
            borderWidth: 0.5,
            backgroundColor: "rgba(221,75,57,0.8)",
            borderColor: "rgba(221,75,57,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.5,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 0.5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(221,75,57,1)",
            pointHoverBorderColor: "rgba(255,255,255,0.5)",
            pointHoverBorderWidth: 0.5,
            pointRadius: 0,
            pointHitRadius: 10,
            data: randomize(Periode),
            spanGaps: false,
      },
   
      {
            label: "Inconnus",
            fill: true,
            lineTension: 0.3,
            borderWidth: 0.5,
            backgroundColor: "rgba(180,180,180,0.4)",
            borderColor: "rgba(180,180,180,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.5,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 0.5,
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(180,180,180,1)",
            pointHoverBorderColor: "rgba(255,255,255,0.5)",
            pointHoverBorderWidth: 0.5,
            pointRadius: 0,
            pointHitRadius: 10,
            data: randomize(Periode),
            spanGaps: false,
      }
    ]  
  };

  var salesChartOptions = {

      display: false,
      scales: {
            yAxes: [{
                display: true,
                stacked: true,
                gridLines: {
                    color: "rgba(0,0,255,0.08)"
                }
            }],
            xAxes: [{
                display: true,
                stacked: true,
                gridLines: {
                    color: "rgba(0,0,255,0.03)"
                }
            }]

        }



  };

  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas,{
    type: 'line',
    data: salesChartData,
    options: salesChartOptions
  });

  //Create the line chart
  //salesChart.Line(salesChartData, salesChartOptions);

  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------

  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  
  var PieData = {
    labels: [
        "Humains",
        "Monétisés",
        "Référencement",
        "Suspects",
        "Bad Bots",
        "Inconnus"
    ],
    datasets: [
        {
            data: [30, 10, 10, 15, 35, 15],
            backgroundColor: [
                "#02B466",
                "#0AEB94",
                "#31B3C7",
                "#F87F28",
                "#DE695B",
                "#E1E1E1"
            ],
            hoverBackgroundColor: [
                "#02B466",
                "#0AEB94",
                "#31B3C7",
                "#F87F28",
                "#DE695B",
                "#E1E1E1"
            ]
        }]
};
  var pieOptions = {
   
     legend: {
                display: false,
                labels: {
                    fontColor: 'rgb(255, 99, 132)'
                }
            }

  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.

  var pieChart = new Chart(pieChartCanvas,{
    type: 'pie',
    data: PieData,
    options: pieOptions
  });


  //pieChart.Doughnut(PieData, pieOptions);
  //-----------------
  //- END PIE CHART -
  //-----------------

  /* jVector Maps
   * ------------
   * Create a world map with markers
   */
  $('#world-map-markers').vectorMap({
    map: 'world_mill_en',
    normalizeFunction: 'polynomial',
    hoverOpacity: 0.7,
    hoverColor: false,
    backgroundColor: 'transparent',
    regionStyle: {
      initial: {
        fill: 'rgba(210, 214, 222, 1)',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      },
      hover: {
        "fill-opacity": 0.7,
        cursor: 'pointer'
      },
      selected: {
        fill: 'yellow'
      },
      selectedHover: {}
    },
    markerStyle: {
      initial: {
        fill: '#00a65a',
        stroke: '#111'
      }
    },
    markers: [
      {latLng: [41.90, 12.45], name: 'Vatican City'},
      {latLng: [43.73, 7.41], name: 'Monaco'},
      {latLng: [-0.52, 166.93], name: 'Nauru'},
      {latLng: [-8.51, 179.21], name: 'Tuvalu'},
      {latLng: [43.93, 12.46], name: 'San Marino'},
      {latLng: [47.14, 9.52], name: 'Liechtenstein'},
      {latLng: [7.11, 171.06], name: 'Marshall Islands'},
      {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
      {latLng: [3.2, 73.22], name: 'Maldives'},
      {latLng: [35.88, 14.5], name: 'Malta'},
      {latLng: [12.05, -61.75], name: 'Grenada'},
      {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
      {latLng: [13.16, -59.55], name: 'Barbados'},
      {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
      {latLng: [-4.61, 55.45], name: 'Seychelles'},
      {latLng: [7.35, 134.46], name: 'Palau'},
      {latLng: [42.5, 1.51], name: 'Andorra'},
      {latLng: [14.01, -60.98], name: 'Saint Lucia'},
      {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
      {latLng: [1.3, 103.8], name: 'Singapore'},
      {latLng: [1.46, 173.03], name: 'Kiribati'},
      {latLng: [-21.13, -175.2], name: 'Tonga'},
      {latLng: [15.3, -61.38], name: 'Dominica'},
      {latLng: [-20.2, 57.5], name: 'Mauritius'},
      {latLng: [26.02, 50.55], name: 'Bahrain'},
      {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
    ]
  });

  /* SPARKLINE CHARTS
   * ----------------
   * Create a inline charts with spark line
   */

  //-----------------
  //- SPARKLINE BAR -
  //-----------------
  $('.sparkbar').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'bar',
      height: $this.data('height') ? $this.data('height') : '30',
      barColor: $this.data('color')
    });
  });

  //-----------------
  //- SPARKLINE PIE -
  //-----------------
  $('.sparkpie').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'pie',
      height: $this.data('height') ? $this.data('height') : '90',
      sliceColors: $this.data('color')
    });
  });

  //------------------
  //- SPARKLINE LINE -
  //------------------
  $('.sparkline').each(function () {
    var $this = $(this);
    $this.sparkline('html', {
      type: 'line',
      height: $this.data('height') ? $this.data('height') : '90',
      width: '100%',
      lineColor: $this.data('linecolor'),
      fillColor: $this.data('fillcolor'),
      spotColor: $this.data('spotcolor')
    });
  });
});
