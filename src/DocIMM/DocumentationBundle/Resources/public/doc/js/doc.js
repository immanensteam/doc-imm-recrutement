/**
 * src/DocIMM/DocumentationBundle/Resources/public/js/doc.js
 * 
 *
 */

/**
 * @type Object Namespace
 */
var doc = {};

/**
 * @type Object Namespace
 */
doc.rpc = {};

/**
 * @type Boolean
 */
doc.rpc.debug = false;

/**
 * Json RPC
 * 
 * Successfull query workflow:
 * 1. beforeSend
 * 2. dataFilter
 * 3. success
 * 4. complete
 * 
 * HTTP error workflow :
 * 1. beforeSend
 * 2. error
 * 3. complete
 * 
 * Other error workflow :
 * 1. beforeSend
 * 2. dataFilter
 * 2. error
 * 3. complete
 * 
 * JSON-RPC error follows the successfull query workflow.
 * 
 * @param String route Symfony route name.
 * @param String method Json-RPC method name.
 * @param Object params Json-RPC method parameters.
 * 
 * @returns Object Json-RPC response object.
 */
doc.rpc.call = function (route, method, params, callback)
{
    var url = Routing.generate(route);
    
    var query = JSON.stringify({
        jsonrpc: '2.0',
        method:  method,
        params:  params,
        id:      1
    });
    
    var data = null;
    
    this.callback = callback;

    $.ajax(url, {
        async:      false,
        type:       'POST',
        data:       query,
        dataType:   'json',
        context:    this,
        beforeSend: this.beforeSend,
        error:      this.error,
        dataFilter: this.dataFilter,
        success:    this.success,
        complete:   this.complete
    });

    return data;
};

/**
 * Before send
 * 
 * Updates the cursor to show activity.
 * 
 * @param Object jqXHR
 * @param Object settings
 * 
 * @returns none
 */
doc.rpc.beforeSend = function (jqXHR, settings)
{
    if (doc.rpc.debug) {
        console.log('$.ajax().beforeSend >', arguments);
    }
    
    $('body').css('cursor', 'progress');

    return;
};

/**
 * Error
 * 
 * Note: The browser already logs the error with URI, HTTP response code and 
 * message.
 * 
 * @param Object jqXHR
 * @param String textStatus
 * @param {type} errorThrown
 * 
 * @returns none
 */
doc.rpc.error = function (jqXHR, textStatus, errorThrown)
{
    console.error('$.ajax().error >', arguments);

    return;
};

/**
 * Data filter
 * 
 * Handles JSON-RPC errors, that are transmitted as normal data from the server.
 * Also abstracts the JSON-RPC container by returning only the payload.
 * 
 * @param String rawData
 * @param String dataType
 * 
 * @returns String|Object
 */
doc.rpc.dataFilter = function (rawData, dataType)
{
    if (doc.rpc.debug) {
        console.log('$.ajax().dataFilter >', arguments);
    }
    
    switch (dataType) {
    
        case 'json':
            var data = JSON.parse(rawData);

            // JSON-RPC errors manifests in regular data
            if (data.error) {

                if (doc.rpc.debug) {
                    console.warn('JSON-RPC error: ', data);
                }
                
                var filteredData = data.error;

            // No error, passes the payload
            } else {

                if (doc.rpc.debug) {
                    console.info('JSON-RPC data: ', data);
                }
                
                var filteredData = data.result;

            }

            return JSON.stringify(filteredData);

        default:
            return rawData;
    }
};

/**
 * Success
 * 
 * Calls the provided callback on the received data.
 * 
 * @param Object data
 * @param String textStatus
 * @param Object jqXHR
 * 
 * @returns none
 */
doc.rpc.success = function (data, textStatus, jqXHR)
{
    if (doc.rpc.debug) {
        console.log('$.ajax().success >', arguments);
    }

    this.callback(data);

    return;
};

/**
 * Complete
 * 
 * Restores the cursor.
 * 
 * @param Object jqXHR
 * @param String result
 * 
 * @returns none
 */
doc.rpc.complete = function (jqXHR, result)
{
    if (doc.rpc.debug) {
        console.log('$.ajax().complete >', arguments);
    }

    $('body').css('cursor', 'default');
    
    return;
};

/**
 * @type Object Namespace
 */
doc.tools = {};

/**
 * Open in modal
 * 
 * @param Object event
 * @param Number clientX
 * @param Number clientY
 * 
 * @returns none
 */
doc.tools.openInModal = function (event, clientX, clientY)
{
    event.preventDefault();
    
    var target = event.currentTarget;

    var modalSize = $(target).data('modal-size');

    var modalTemplate =
        '<div id="modal" class="modal fade">' +
        '  <div class="modal-dialog ' + modalSize + '">' +
        '    <div class="modal-content">' +
        '    </div>' +
        '  </div>' +
        '</div>';

    // Append it to the DOM berfore binding events to it.
    $('body').append(modalTemplate);

    $('#modal')
        .modal()

        // This event fires immediately when the show instance method is called.
        // If caused by a click, the clicked element is available as the 
        // relatedTarget property of the event.
        .on('show.bs.modal', function(event) {
            // console.debug('show.bs.modal', target);
           
            $('#modal').find('.modal-content').load(
                target.href /*,
                checkSessionTimeout*/
            );
            $(this).off('show.bs.modal');
        })

        // This event is fired when the modal has been made visible to the user 
        // (will wait for CSS transitions to complete). If caused by a click, 
        // the clicked element is available as the relatedTarget property of the 
        // event.
        .on('shown.bs.modal', function(event) {
            // console.debug('shown.bs.modal', arguments);
            $(this).off('shown.bs.modal');
        })

        // This event is fired immediately when the hide instance method has 
        // been called.
        .on('hide.bs.modal', function(event) {
            // console.debug('hide.bs.modal', arguments);
            $(this).off('hide.bs.modal');
        })

        // This event is fired when the modal has finished being hidden from the 
        // user (will wait for CSS transitions to complete).
        .on('hidden.bs.modal', function(event) {
            // console.debug('hidden.bs.modal', arguments);

            $('#modal').remove();
            $('.modal-backdrop').remove();
            
            $(this).off('hidden.bs.modal');
        })

        // This event is fired when the modal has loaded content using the 
        // remote option.
        .on('loaded.bs.modal', function(event) {
            // console.debug('loaded.bs.modal', arguments);
    
            $(this).off('loaded.bs.modal');
        })

        // Finally show it
        .modal('show')
    ;

    return;
};
