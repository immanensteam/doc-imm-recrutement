/**
 * src/DocIMM/DocumentationBundle/Resources/public/js/doc.admin.entities.js
 * 
 * @copyright Immanens
 * @author David Mézière <d.meziere@immanens.com>
 */

/**
 * @type Object Namespace
 */
doc.admin.entities = {};

/**
 * Entities index action
 * 
 * @returns none
 */
doc.admin.entities.index = function ()
{
    var entity = $('#entities').data('entity');
    
    window.table = $('#entities').DataTable({
        language: {
            url: Routing.generate('admin_default_index') + 'bundles/core/js/datatables.' + Translator.locale + '.json'
        },
        processing: true,
        serverSide: true,
        ajax: function (data, callback, settings) {
            
            data.entity = entity;
            
            $.ajax({
                contentType: 'application/json',
                url: Routing.generate('admin_entities_rpc'),
                type: 'post',
                data: JSON.stringify({jsonrpc: '2.0', method: 'search', params: data, id: data.draw}),
                success: function (resultsFromServer) {
                    callback(resultsFromServer.result);
                }
            });
        },
        columnDefs: doc.admin.entities.cols[entity]
    });
    
    return;
};

/**
 * @type Object Namespace
 */
doc.admin.entities.cols = {};

/**
 * User columns definition
 * 
 * @type Array
 */
doc.admin.entities.cols.User = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'User', 
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }},
    {targets: 2, render: $.fn.dataTable.render.moment('X', 'DD/MM/YYYY', 'fr')}
];

/**
 * Publication columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Publication = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Publication',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Catalog columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Catalog = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Catalog',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * CatalogPublication columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.CatalogPublication = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'CatalogPublication',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Category columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Category = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Category',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Store columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Store = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Store',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Brand columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Brand = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Brand',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Coupon columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Coupon = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Coupon',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Coverflow columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Coverflow = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Coverflow',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Issue columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Issue = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Issue',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }},
    {targets: 3, render: $.fn.dataTable.render.moment('X', 'DD/MM/YYYY', 'fr')}
];

/**
 * Customer columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Customer = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Customer',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Device columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Device = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Device',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * DeviceIssue columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.DeviceIssue = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'DeviceIssue',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }},
    {targets: 3, render: $.fn.dataTable.render.moment('X', 'DD/MM/YYYY hh:mm', 'fr')}
];

/**
 * Offer columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Offer = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Offer',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }},
    {targets: 6, render: $.fn.dataTable.render.moment('X', 'DD/MM/YYYY', 'fr')},
    {targets: 7, render: $.fn.dataTable.render.moment('X', 'DD/MM/YYYY', 'fr')}
];

/**
 * Option columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.Option = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'Option',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * CategoryPublication columns definition
 *
 * @type Array
 */
doc.admin.entities.cols.CategoryPublication = [
    {targets: 0, render: function (data, type, full, meta) {
        return '<a href="' + Routing.generate('admin_entities_edit', {entityName: 'CategoryPublication',
            entityId: full['DT_RowId']}) + '">' + full['DT_RowId'] + '</a>';
    }}
];

/**
 * Entities new action
 * 
 * @returns none
 */
doc.admin.entities.new = function ()
{
    $('.input-group.date').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: Translator.locale,
        ignoreReadonly: true
    });

    return;
};

/**
 * Entities edit action
 * 
 * @returns none
 */
doc.admin.entities.edit = function ()
{
    $('.input-group.date').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: Translator.locale,
        ignoreReadonly: true
    });

    $('button.btn-danger').click(function(event) {
        event.preventDefault();
        if (confirm(Translator.trans('edit.deleteWarning', {}, 'AdminEntities'))) {
            $('#delete_form').submit();
        }
        
        return;
    });
    
    return;
};
