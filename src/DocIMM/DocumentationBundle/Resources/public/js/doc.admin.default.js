/**
 * src/DocIMM/DocumentationBundle/Resources/public/js/doc.admin.default.js
 * 
 * @copyright Immanens
 * @author David Mézière <d.meziere@immanens.com>
 */

/**
 * @type Object Namespace
 */
doc.admin.default = {};

/**
 * Default index action
 * 
 * @return none
 */
doc.admin.default.index = function ()
{
    return;
};
