<?php

namespace DocIMM\DocumentationBundle\Controller;

// Framework elements
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\hasJsonRpc;
use Symfony\Component\HttpFoundation\JsonResponse;

// Annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityManager;

use AppBundle\Entity\Token;

/**
 * DocIMM admin bundle default controller
 *
 * @Route("/")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
 */
class DefaultController extends Controller
{
    /**
     * @var EntityManager Doctrine entity manager
     */
    private $em;


    /**
     * Index action
     *
     * @Method({"GET"})
     * @Route("/", name="admin_default_index", options={"expose"="true"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array Template parameters
     */
    public function indexAction(Request $request)
    {
        $this->postToken($request->getClientIp());
        if($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            return $this->render('DocumentationBundle:Default:index.html.twig');
        } else {
            return $this->redirectToRoute('user_list_doc',array('entityName' => 'Documentation'));
        }
    }

    private function postToken($ip){

        $em = $this->getDoctrine()->getManager();
        $now = new \DateTime('now');

        $token = $em->getRepository('AppBundle:Token')->findOneByUser($this->getUser());
        if(is_object($token)){
            if(date_format($token->getAvailableUntil(), 'Y-m-d H:i:s ') < date_format($now, 'Y-m-d H:i:s ')){
                $token->setIp($ip);
                $token->setToken(md5(uniqid()));
                $token->setCreatedAt(new \DateTime('now'));
                $token->setAvailableUntil(new \DateTime('now +1 day'));
                $em->flush();
            }

        }else{
            $authToken = new Token();
            $authToken->setIp($ip);
            $authToken->setToken(md5(uniqid()));
            $authToken->setCreatedAt($now);
            $authToken->setAvailableUntil(new \DateTime('now +1 day'));
            $authToken->setUser($this->getUser());
            $em->persist($authToken);
            $em->flush();

        }

    }

    /**
     * Will also have JSON-RPC related methods :
     * - jRpc()
     * - jRpcHandleQuery()
     * - jRpcError)
     */
    use hasJsonRpc;
    /**
     * Check token action
     *
     * @Security("'is_anonymous()' or 'is_authenticated'")
     * @Method({"POST"})
     * @Route("/checktoken", name="token_check", options={"expose"="true"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkTokenAction(Request $request)
    {
        $ip= $request->get('ip');
        $token=$request->get('token');
        $doc_name=$request->get('doc');
        $now = new \DateTime('now');
        $response = false;
        $check_token = $this->getDoctrine()->getRepository('AppBundle:Token')->findOneBy(array('ip' => $ip,'token' => $token) );
        if(is_object($check_token)) {
            if (date_format($check_token->getAvailableUntil(), 'Y-m-d H:i:s ') > date_format($now, 'Y-m-d H:i:s ')) {
                $documentation = $this->getDoctrine()->getRepository('AppBundle:Documentation')->findOneByName($doc_name);
                $userManager = $this->get('fos_user.user_manager');
                $user =  $userManager->findUserBy(array('id'=>$check_token->getUser()->getId()));
                $user_docs = $user->getDocumentations();
                $docs_array = array();
                foreach ($user_docs as $key => $doc) {
                    $docs_array[] = $doc->getId();
                }
                if(in_array($documentation->getId(), $docs_array)){
                   $response = true; 
                } else {
                    $response = "noright";
                }
            }
        }

        $json_response = new Response();

        $json_response->setContent(json_encode([
            'checktoken' => $response
        ]));

        $json_response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $json_response->headers->set('Access-Control-Allow-Origin', '*');

        return $json_response;
    }

    /**
     * Check ip action
     *
     * @Security("'is_anonymous()' or 'is_authenticated'")
     * @Method({"GET"})
     * @Route("/checkip", name="checkip", options={"expose"="true"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkipAction(Request $request)
    {
        $ip = $request->getClientIp();

        $response = new Response();

        $response->setContent(json_encode([
            'ip' => $request->getClientIp()
        ]));

        $response->headers->set('Content-Type', 'application/json');
        // Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }

}
