<?php
/**
 * src/DocIMM/DocumentationBundle/Controller/EntitiesController.php
 */

namespace DocIMM\DocumentationBundle\Controller;

// Framework elements
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\hasJsonRpc;

// Annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 *
 * @Route("/entities")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
 */
class EntitiesController extends Controller
{
    /**
     * Will also have JSON-RPC related methods :
     * - jRpc()
     * - jRpcHandleQuery()
     * - jRpcError)
     */
    use hasJsonRpc;

    /**
     * RPC Action
     *
     * @Method({"POST"})
     * @Route(".json", name="admin_entities_rpc", options={"expose"="true"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function rpcAction(Request $request)
    {
        return $this->jRpc($request);
    }

    /**
     * Search
     *
     * @param stdClass JSON-RPC request
     * @param stdClass JSON-RPC response
     *
     * @return stdClass JSON-RPC response
     */
    protected function search($request, $response)
    {
        $entity = strtolower($request->params->entity);

        $request->params->request = 'admin';

        return $this->get('admin.'.$entity)->search($request, $response);
    }

    /**
     * Index action
     *
     * Lists all entities of provided name.
     *
     * @Method("GET")
     * @Route("/{entityName}.html", name="admin_entities_index")
     * @Template()
     *
     * @param string $entityName Entity name
     *
     * @return array Template parameters
     */
    public function indexAction($entityName)
    {
        $params = [];

        $em = $this->getDoctrine()->getManager();

        $params['entityName'] = $entityName;

        $params['count'] = $em->getRepository('AppBundle:'.$entityName)->count(strtolower(substr($entityName, 0, 1)));

        $params['tabs'] = $em->getRepository('AppBundle:'.$entityName)->findAll();

        return $params;
    }


    /**
     * Index action
     *
     * Lists all entities of provided name.
     *
     * @Method("GET")
     * @Route("/list/{entityName}.html", name="user_list_doc")
     * @Template()
     *
     * @param string $entityName Entity name
     *
     * @return array Template parameters
     */
    public function listAction($entityName)
    {
        $params = [];
        $user = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();

        $params['entityName'] = $entityName;

        $params['count'] = $em->getRepository('AppBundle:'.$entityName)->count(strtolower(substr($entityName, 0, 1)));

        $params['tabs'] = $this->getUser()->getDocumentations();
        $params['token'] = $em->getRepository('AppBundle:Token')->findOneByUser($this->getUser());

        return $params;
    }




    /**
     * New action
     *
     * Creates a new alert entity.
     *
     * @Method({"GET", "POST"})
     * @Route("/{entityName}/new.html", name="admin_entities_new")
     * @Template()
     *
     * @param Request $request
     * @param string $entityName Entity name
     *
     * @return RedirectResponse to admin_entities_index
     * @return array Template parameters
     */
    public function newAction(Request $request, $entityName)
    {
        $entityFullName = 'AppBundle\\Entity\\'.$entityName;
        $typeFullName   = 'DocIMM\\DocumentationBundle\\Form\\'.$entityName.'Type';

        $entity = new $entityFullName();
        $form = $this->createForm($typeFullName, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->get('admin.'.strtolower($entityName))->create($entity);

            return $this->redirectToRoute('admin_entities_index', array(
                'entityName' => $entityName
            ));
        }

        return array(
            'entityName' => $entityName,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Edit action
     *
     * Displays a form to edit an existing alert entity.
     *
     * @Method({"GET", "POST"})
     * @Route("/{entityName}/{entityId}/edit.html", name="admin_entities_edit", options={"expose"="true"})
     * @Template()
     *
     * @param Request $request
     * @param string $entityName Entity name
     * @param integer $entityId Entity id
     *
     * @return RedirectResponse to admin_entities_index
     * @return array Template parameters
     */
    public function editAction(Request $request, $entityName, $entityId)
    {
        $entity = $this->getDoctrine()->getRepository('AppBundle:'.$entityName)->find($entityId);
        $typeFullName = 'DocIMM\\DocumentationBundle\\Form\\'.$entityName.'Type';

        $deleteForm = $this->createDeleteForm($entityName, $entity);

        $editForm = $this->createForm($typeFullName, $entity);

        if ($entityName === 'User') {
            $editForm->remove('plainPassword');
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->get('admin.'.strtolower($entityName))->update($entity);

            return $this->redirectToRoute('admin_entities_index', array(
                'entityName' => $entityName
            ));
        }

        return array(
            'entityName'  => $entityName,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Delete action
     *
     * Deletes a alert entity.
     *
     * @Method("DELETE")
     * @Route("/{entityName}/{entityId}/delete", name="admin_entities_delete")
     *
     * @param Request $request
     * @param string $entityName Entity name
     * @param integer $entityId Entity id
     *
     * @return RedirectResponse to admin_entities_index
     */
    public function deleteAction(Request $request, $entityName, $entityId)
    {
        $entity = $this->getDoctrine()->getRepository('AppBundle:'.$entityName)->find($entityId);

            $this->get('admin.'.strtolower($entityName))->delete($entity);

        return $this->redirectToRoute('admin_entities_index', array(
            'entityName' => $entityName
        ));
    }

    /**
     * Creates a form to delete a alert entity.
     *
     * @param string $entityName Entity name
     * @param mixed $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($entityName, $entity)
    {
        return $this->createFormBuilder(null, array('attr' => array('id' => 'delete_form')))
            ->setAction($this->generateUrl('admin_entities_delete', array(
                'entityName' => $entityName,
                'entityId'   => $entity->getId()
            )))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
