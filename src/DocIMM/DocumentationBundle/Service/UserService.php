<?php
/**
 * src/DocIMM/DocumentationBundle/Service/UserService.php
 */


namespace DocIMM\DocumentationBundle\Service;

use Doctrine\ORM\EntityManager;

// Business entities
use AppBundle\Entity\User;


class UserService
{
    /**
     * @var EntityManager Doctrine entity manager
     */
    private $em;

    /**
     * Construct
     *
     * @param EntityManager $em
     *
     * @return none
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        return;
    }

    /**
     * Search
     *
     * @param $request stdClass JSON-RPC request
     * @param $response stdClass JSON-RPC response
     *
     * @return array
     */
    public function search($request, $response)
    {
        $repository = $this->em->getRepository('AppBundle:User');

        $users = $repository->search($request->params);

        $count = $repository->searchCount($request->params, 'u');

        $response->result = array(
            'draw'            => $request->params->draw,
            'recordsFiltered' => $count,
            'recordsTotal'    => $count,
            'data'            => array()
        );

        if (count($users) === 0) {
            $response->result['error'] = 'Aucun résultat';
        }

        foreach ($users as $user) {

            $response->result['data'][] = array(
                'DT_RowId' => $user['id'],
                0 => $user['id'],
                1 => $user['username'],
                2 => ($user['lastLogin'] === null ? '' : $user['lastLogin']->getTimestamp())
            );
        }

        return $response;
    }

    /**
     * Create
     *
     * @param User $user
     *
     * @return User
     */
    public function create(User $user)
    {
        $user->setUsername($user->getEmail());
        $user->setEnabled(true);
        $this->em->persist($user);
        $this->em->flush($user);

        return $user;
    }

    /**
     * Update
     *
     * @param User $user
     *
     * @return User
     */
    public function update(User $user)
    {
        $user->setUsername($user->getEmail());
        $this->em->flush($user);

        return $user;
    }

    /**
     * Delete
     *
     * @param User $user
     *
     * @return User
     */
    public function delete(User $user)
    {
        $this->em->remove($user);
        $this->em->flush($user);

        return $user;
    }
}
