<?php
/**
 * src/DocIMM/DocumentationBundle/Service/DocumentationService.php
 */
namespace DocIMM\DocumentationBundle\Service;

use AppBundle\Entity\Documentation;
use Doctrine\ORM\EntityManager;

class DocumentationService
{
    /**
     * @var EntityManager Doctrine entity manager
     */
    private $em;

    /**
     * Construct
     *
     * @param EntityManager $em
     *
     * @return none
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        return;
    }

    /**
     * Search
     *
     * @return array
     */
    public function search($request, $response)
    {
        $repository = $this->em->getRepository('AppBundle:Documentation');

        $documentations = $repository->search($request->params);

        $count = $repository->searchCount($request->params, 'u');

        $response->result = array(
            'draw'            => $request->params->draw,
            'recordsFiltered' => $count,
            'recordsTotal'    => $count,
            'data'            => array()
        );

        if (count($documentations) === 0) {
            $response->result['error'] = 'Aucun résultat';
        }

        foreach ($documentations as $documentation) {

            $response->result['data'][] = array(
                'DT_RowId' => $documentation['id'],
                0 => $documentation['id'],
                1 => $documentation['name'],
                2 => $documentation['url']
            );
        }

        return $response;
    }

    /**
     * Create
     *
     * @param Documentation $documentation
     *
     * @return Documentation
     */
    public function create(Documentation $documentation)
    {
        $this->em->persist($documentation);
        $this->em->flush($documentation);

        return $documentation;
    }

    /**
     * Update
     *
     * @param Documentation $documentation
     *
     * @return Documentation
     */
    public function update(Documentation $documentation)
    {
        $this->em->flush($documentation);

        return $documentation;
    }

    /**
     * Delete
     *
     * @param Documentation $documentation
     *
     * @return Documentation
     */
    public function delete(Documentation $documentation)
    {
        $this->em->remove($documentation);
        $this->em->flush($documentation);

        return $documentation;
    }
}
