<?php
/**
 * src/BotsCorner/UserBundle/Service/AfterLoginRedirection.php
 */

namespace BotsCorner\UserBundle\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;


class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * Constructor
     *
     * @param RouterInterface $router
     *
     * @return none
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;

        return;
    }

    /**
     * On authentication success
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // Get list of roles for current user
        $roles = $token->getRoles();

        // Tranform this list in array
        $rolesTab = array_map(function($role) {
            return $role->getRole();
        }, $roles);

        // If is a admin or user we redirect to the backoffice area
        if (in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_USER', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('admin_default_index'));

            // Otherwise, if is an editor user we redirect to the dedicated area
        }
        return $redirection;
    }
}
