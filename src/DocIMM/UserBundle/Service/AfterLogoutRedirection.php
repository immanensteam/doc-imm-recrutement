<?php


namespace DocIMM\UsersBundle\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

/**
 * After logout redirection
 */
class AfterLogoutRedirection implements LogoutSuccessHandlerInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorage
     */
    private $security;

    /**
     * Constructor
     *
     * @param RouterInterface $router
     * @param SecurityContextInterface $security
     *
     * @return none
     */
    public function __construct(RouterInterface $router, TokenStorage $security)
    {
        $this->router = $router;
        $this->security = $security;

        return;
    }

    /**
     * On logout success
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        // Get list of roles for current user
        $roles = $this->security->getToken()->getRoles();

        // Tranform this list in array
        $rolesTab = array_map(function($role){
            return $role->getRole();
        }, $roles);

        // If is a admin or  user we redirect to the login area. Here we used FoseUserBundle bundle
        if ( in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_USER', $rolesTab, true)) {
            $response = new RedirectResponse($this->router->generate('fos_user_security_login'));

            // otherwise we redirect user to the homepage of website
        } else {
            $response = new RedirectResponse($this->router->generate('homepage'));
        }

        return $response;
    }
}
