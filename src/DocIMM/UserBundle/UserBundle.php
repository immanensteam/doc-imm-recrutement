<?php

namespace DocIMM\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UserBundle extends Bundle
{

    /**
     * Get parent
     *
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
